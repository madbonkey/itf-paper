Jo,

I'm coming over to your office in a little bit with a new computer to swap out for your broken down computer.  I'll diagnose the problems from my desk.

Thanks,

Terry
IT Administrator, M57.biz
terry@m57.biz
831-233-2883
  ----- Original Message ----- 
  From: Jo Smith 
  To: Terry Johnson 
  Sent: Friday, November 20, 2009 9:55 AM
  Subject: Re: computer problem


  Terry,

  Did you fix my computer yet?  It is still running way slow.  Thanks.

  - Jo
    ----- Original Message ----- 
    From: Terry Johnson 
    To: Jo Smith 
    Sent: Wednesday, November 18, 2009 10:28 AM
    Subject: Re: computer problem


    Jo,

    I'll stop by to check out the issue in a few minutes. 

    Thanks,

    Terry
    IT Administrator, M57.biz
    terry@m57.biz
    831-233-2883
      ----- Original Message ----- 
      From: Jo Smith 
      To: Terry Johnson 
      Sent: Wednesday, November 18, 2009 10:27 AM
      Subject: computer problem


      Terry,

      Can you come by and take a look at my computer?  It's gotten really slow all of a sudden.  Thanks.

      Jo