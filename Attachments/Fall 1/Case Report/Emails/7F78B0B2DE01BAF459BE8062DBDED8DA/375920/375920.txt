Jo,

   yes, I would be concerned about that too, thanks for thinking about that.

Terry - what did you /are you going to/ do with Jo's computer?

We need to make sure it is properly erased !  Thank you.

Pat
----- Original Message ----- 
From: <jo@m57.biz>
To: "Pat McGoo" <pat@m57.biz>
Sent: Friday, November 20, 2009 2:29 PM
Subject: Equipment Disposal


> Pat,
>
> My computer had to be swapped out today.  I just want to make sure it is
> properly disposed of.  There could be company information on there that we
> don't want to share with the rest of the world.  Right?
>
> - Jo
> 
