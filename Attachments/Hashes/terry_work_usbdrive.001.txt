﻿Created By AccessData® FTK® Imager 3.0.1.1467 110406

Case Information: 
Acquired using: ADI3.0.1.1467
Case Number: MT-2009-12-11
Evidence Number: 8
Unique description: Terry's USB
Examiner: Martin
Notes:  

--------------------------------------------------------------

Information for C:\Users\Forensik-Admin\Desktop\HDD\TerrysUSB:

Physical Evidentiary Item (Source) Information:
[Drive Geometry]
 Bytes per Sector: 512
 Sector Count: 4.095.882
 Source data size: 1999 MB
 Sector count:    4095882
[Computed Hashes]
 MD5 checksum:    9d84f913f3d056e45bd82ed78aa9ba6f
 SHA1 checksum:   2cf7a2465d3e72e6b5b0f5fcfd6914ff9e4cf334

Image Information:
 Acquisition started:   Mon May 09 12:31:15 2011
 Acquisition finished:  Mon May 09 12:37:52 2011
 Segment list:
  C:\Users\Forensik-Admin\Desktop\HDD\terry_work_usbdrive.001

Image Verification Results:
 Verification started:  Mon May 09 12:37:52 2011
 Verification finished: Mon May 09 12:38:02 2011
 MD5 checksum:    9d84f913f3d056e45bd82ed78aa9ba6f : verified
 SHA1 checksum:   2cf7a2465d3e72e6b5b0f5fcfd6914ff9e4cf334 : verified
